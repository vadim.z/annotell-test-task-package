from typing import Dict
from typing import List
from typing import Optional

from pydantic import BaseModel
from pydantic.types import conlist


class ElementObjectTypeSchema(BaseModel):
    name: str
    type: str


class ElementObjectRelationRDFObjectSchema(BaseModel):
    type: str
    uid: str


class ElementObjectRelationRDFSubjectsSchema(ElementObjectRelationRDFObjectSchema):
    ...


class ElementObjectRelationSchema(BaseModel):
    name: str
    type: str
    rdf_objects: List[ElementObjectRelationRDFObjectSchema]
    rdf_subjects: List[ElementObjectRelationRDFSubjectsSchema]


class ElementsSchema(BaseModel):
    objects: Dict[str, ElementObjectTypeSchema]
    relations: Dict[str, ElementObjectRelationSchema]


class FrameObjectDataBBoxSchema(BaseModel):
    name: str
    stream: str
    val: conlist(float, min_items=4, max_items=4)


class FrameObjectDataBooleanSchema(BaseModel):
    name: str
    val: bool


class FrameObjectDataTextSchema(BaseModel):
    name: str
    val: str


class FrameObjectDataSchema(BaseModel):
    bbox: List[FrameObjectDataBBoxSchema]
    boolean: Optional[List[FrameObjectDataBooleanSchema]]
    text: Optional[List[FrameObjectDataTextSchema]]


class FrameObjectsDataSchema(BaseModel):
    object_data: FrameObjectDataSchema


class FramesSchema(BaseModel):
    objects: Dict[str, FrameObjectsDataSchema]
    relations: Dict[str, dict]


class OpenLabelSchema(BaseModel):
    elements: ElementsSchema
    frames: Dict[str, FramesSchema]
