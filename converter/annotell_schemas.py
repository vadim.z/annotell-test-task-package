from typing import Dict
from typing import List
from typing import Optional

from pydantic import BaseModel
from pydantic import validator
from pydantic.types import conlist


class CoordinatesSchema(BaseModel):
    coordinates: conlist(float, max_items=2)
    visible: bool


class GeometryCoordinatesSchema(BaseModel):
    maxX: CoordinatesSchema
    maxY: CoordinatesSchema
    minX: CoordinatesSchema
    minY: CoordinatesSchema


class FeatureGeometrySchema(BaseModel):
    coordinates: GeometryCoordinatesSchema
    type: str


class FeatureSchema(BaseModel):
    geometry: FeatureGeometrySchema
    id: str
    properties: dict
    type: str


class ShapeSchema(BaseModel):
    type: str
    features: List[FeatureSchema]


class ShapePropertySchema(BaseModel):
    class_: str
    ObjectType: Optional[str]
    Unclear: Optional[bool]
    PushingOrPulling: Optional[str]

    @validator('PushingOrPulling', pre=True)
    def get_pushing_or_pulling_value(cls, v):
        if isinstance(v, str):
            return v
        if isinstance(v, dict) and isinstance(shape := v.get('shape'), str):
            return shape

        raise ValueError('`PushingOrPulling` must be either string or dictionary with `shape` key and string value.')

    class Config:
        fields = {
            'class_': 'class'
        }


class AnnotellAnnotationSchema(BaseModel):
    certainty: str
    internalProperties: dict
    properties: dict
    shapeProperties: Dict[str, Dict[str, ShapePropertySchema]]
    shapes: Dict[str, ShapeSchema]
