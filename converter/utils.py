from typing import Tuple


def calculate_middle_point(x1: float, y1: float, x2: float, y2: float) -> Tuple[float]:
    """
        Calculates middle point between two coordinates.
    """
    return ((x1 + x2) / 2, (y1 + y2) / 2)
