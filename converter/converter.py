import logging
from uuid import uuid4

from .annotell_schemas import AnnotellAnnotationSchema
from .open_label_schemas import ElementObjectRelationRDFObjectSchema
from .open_label_schemas import ElementObjectRelationRDFSubjectsSchema
from .open_label_schemas import ElementObjectRelationSchema
from .open_label_schemas import ElementObjectTypeSchema
from .open_label_schemas import ElementsSchema
from .open_label_schemas import FrameObjectDataBBoxSchema
from .open_label_schemas import FrameObjectDataBooleanSchema
from .open_label_schemas import FrameObjectDataSchema
from .open_label_schemas import FrameObjectDataTextSchema
from .open_label_schemas import FrameObjectsDataSchema
from .open_label_schemas import FramesSchema
from .open_label_schemas import OpenLabelSchema
from .utils import calculate_middle_point

logger = logging.getLogger(__name__)


def to_open_label_annotation(data: dict) -> dict:
    annotell_annotation = AnnotellAnnotationSchema.parse_obj(data)

    elements = {}
    relations = {}
    frame_relations = {}
    frames_objects = {}
    properties = {}
    for i, (shape_uuid, property) in enumerate(annotell_annotation.shapeProperties.items()):
        pushing_or_pulling = property['@all'].PushingOrPulling
        properties[shape_uuid] = {
            'ObjectType': property['@all'].ObjectType,
            'Unclear': property['@all'].Unclear,
            'PushingOrPulling': pushing_or_pulling
        }
        elements[shape_uuid] = ElementObjectTypeSchema(name=shape_uuid, type=property['@all'].class_)
        if pushing_or_pulling:
            frame_relations[len(relations)] = {}
            relation_name = str(i)
            relations[relation_name] = ElementObjectRelationSchema(
                name=relation_name,
                type='PushingOrPulling',
                rdf_objects=[ElementObjectRelationRDFObjectSchema(type='object', uid=shape_uuid)],
                rdf_subjects=[ElementObjectRelationRDFSubjectsSchema(type='object', uid=pushing_or_pulling)]
            )
    for shape_group_name, features in annotell_annotation.shapes.items():
        for feature in features.features:
            coordinates = feature.geometry.coordinates
            shape_uuid = feature.id
            frames_objects[shape_uuid] = FrameObjectsDataSchema(
                object_data=FrameObjectDataSchema(
                    bbox=[FrameObjectDataBBoxSchema(
                        name=f'bbox-{uuid4()}',  # Assuming that bbox name is random generated string.
                        stream=shape_group_name,
                        val=[
                            *calculate_middle_point(*coordinates.maxX.coordinates, *coordinates.minX.coordinates),
                            *calculate_middle_point(*coordinates.maxY.coordinates, *coordinates.minY.coordinates)
                        ]
                    )],
                    boolean=[
                        FrameObjectDataBooleanSchema(name=name, val=value)
                        for name, value in properties.get(shape_uuid, []).items() if isinstance(value, bool)
                    ],
                    text=[
                        FrameObjectDataTextSchema(name=name, val=value)
                        for name, value in properties.get(shape_uuid, []).items() if isinstance(value, str)
                    ],
                )
            )

    return {
        'data': {
            'openlabel': OpenLabelSchema(
                elements=ElementsSchema(
                    objects=elements,
                    relations=relations
                ),
                frames={'': FramesSchema(
                    objects=frames_objects,
                    relations=frame_relations
                )}
            ).dict()
        }
    }
