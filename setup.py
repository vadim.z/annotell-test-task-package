import setuptools


setuptools.setup(
    name='Annotell data converter',
    version='0.0.1',
    author='Vadim Zabolotniy',
    author_email='vadym.zabolotnyi@beetroot.se',
    description='Converter to transform annotell annotation to OpenLabel.',
    url='https://gitlab.com/vadim.z/annotell-test-task-package',
    license='GPL',
    packages=['converter'],
    install_requires=[
        'pydantic'
    ],
)
