import json
from pprint import pprint

from converter import to_open_label_annotation

with open('annotell_1.json') as file:
    parsed_data = json.loads(file.read())
    pprint(to_open_label_annotation(parsed_data))
