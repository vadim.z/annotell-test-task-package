# Annotell Test Task PIP Package

## Prerequrements
Project tested with `Python 3.8`. To deploy it you will need `virtualenv` and
`make`.

## Installing dependencies
To setup environment run command `make deploy`. It will create virtual
environment in `.venv` directory inside project root and install all required
packages.

## Running example
You can find package usage example in `usage_example.py` in root of the project.
To run it execute command `make run`. It will convert Annotell annotation data
from `annotell_1.json` file to Open Label annotation format. Result of
convertation you should see in terminal.
